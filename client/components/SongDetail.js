import React from "react";
import { graphql } from "react-apollo";
import { Link } from "react-router-dom";

import fetchSong from "../queries/fetchSong";
import LyricCreate from "./LyricCreate";
import LyricList from "./LyricList";

class SongDetail extends React.Component {
  render() {
    if (this.props.data.loading) {
      return <div>Loading ...</div>;
    }

    if (!this.props.data.song) {
      this.props.history.push("/");
    }

    const { title, lyrics } = this.props.data.song;

    return (
      <div className="container">
        <Link to="/">Back</Link>
        <h3>{title}</h3>
        <LyricList lyrics={lyrics} />
        <LyricCreate id={this.props.match.params.id} />
      </div>
    );
  }
}

export default graphql(fetchSong, {
  options: ({ match }) => ({ variables: { id: match.params.id } })
})(SongDetail);
