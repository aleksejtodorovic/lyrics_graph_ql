import React from "react";
import { graphql } from "react-apollo";

import likeLyric from "../mutations/likeLyric";

class LyricList extends React.Component {
  onLike = (id, likes) => {
    this.props.mutate({
      variables: {
        id
      },
      optimisticResponse: {
        __typename: "Mutation",
        likeLyric: {
          id,
          __typename: "LyricType",
          likes: likes + 1
        }
      }
    });
  };

  renderLyrics() {
    return this.props.lyrics.map(({ id, content, likes }) => {
      return (
        <li key={id} className="collection-item">
          {content}
          <div className="vote-box">
            <i
              className="material-icons blue-text"
              onClick={() => this.onLike(id, likes)}
            >
              thumb_up
            </i>
            {likes} like{likes !== 1 ? "s" : ""}
          </div>
        </li>
      );
    });
  }

  render() {
    const { lyrics } = this.props;

    return (
      <ul
        className="collection"
        style={{ display: `${lyrics.length > 0 ? "block" : "none"}` }}
      >
        {this.renderLyrics()}
      </ul>
    );
  }
}

export default graphql(likeLyric)(LyricList);
