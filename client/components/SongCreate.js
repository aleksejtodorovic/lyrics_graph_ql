import React from "react";
import { graphql } from "react-apollo";
import { Link } from "react-router-dom";

import fetchSongs from "../queries/fetchSongs";
import addSong from "../mutations/addSong";

class SongCreate extends React.Component {
  state = { title: "" };

  onSubmit = async evt => {
    evt.preventDefault();

    await this.props.mutate({
      variables: {
        title: this.state.title
      },
      refetchQueries: [{ query: fetchSongs }]
    });

    this.props.history.push("/");
  };

  render() {
    return (
      <div className="container">
        <Link to="/">Back</Link>
        <h3>Create a New Song</h3>
        <form onSubmit={this.onSubmit}>
          <label>Song Title: </label>
          <input
            value={this.state.title}
            onChange={({ target }) => this.setState({ title: target.value })}
          />
        </form>
      </div>
    );
  }
}

export default graphql(addSong)(SongCreate);
