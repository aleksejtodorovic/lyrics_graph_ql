import React from "react";
import { graphql } from "react-apollo";

import AddLyric from "../mutations/addLyrics";

class LyricCreate extends React.Component {
  state = { content: "" };

  onSubmit = async evt => {
    evt.preventDefault();

    await this.props.mutate({
      variables: {
        songId: this.props.id,
        content: this.state.content
      }
    });

    this.setState({ content: "" });
  };

  render() {
    return (
      <form onSubmit={this.onSubmit}>
        <label>Add Lyric: </label>
        <input
          value={this.state.content}
          onChange={({ target }) => this.setState({ content: target.value })}
        />
      </form>
    );
  }
}

export default graphql(AddLyric)(LyricCreate);
