import "./style/style.css";
import React from "react";
import { render } from "react-dom";
import ApolloClient from "apollo-client";
import { ApolloProvider } from "react-apollo";
import { HashRouter as Router, Route } from "react-router-dom";

import App from "./components/App";
import SongCreate from "./components/SongCreate";
import SongDetail from "./components/SongDetail";

const client = new ApolloClient({
  dataIdFromObject: ({ id }) => id // will be called for each object
});

const Root = () => {
  return (
    <ApolloProvider client={client}>
      <Router>
        <div>
          <Route exact path="/" component={App} />
          <Route path="/songs/new" component={SongCreate} />
          <Route path="/songs/:id" component={SongDetail} />
        </div>
      </Router>
    </ApolloProvider>
  );
};

render(<Root />, document.querySelector("#root"));
